import React from "react";
import './App.css';
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import * as Routes from "./routes";
import Login from "./views/login";
import Main from "./views/main";
import AuthenticatedUserContext from './context'
import {authenticatedUser} from './context'
import {Provider} from 'react-redux'
import {createStore} from "redux";

import rootReducers from "./redux/reducers";

const store = createStore(rootReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

function App() {
  const [userAuthenticated, setUserAuthenticated] = React.useState(authenticatedUser)
  const clearUser = ()=> {
    setUserAuthenticated({})
    sessionStorage.setItem("userAuthenticated", JSON.stringify({}))
  }
  return (
    <Provider store={store}>

      <AuthenticatedUserContext.Provider value={userAuthenticated}>
        <AuthenticatedUserContext.Consumer>
          {user => {
            let sessionUser = !sessionStorage.getItem("userAuthenticated")?{}: JSON.parse(sessionStorage.getItem("userAuthenticated"));
            let userMerged = {};
            if(!sessionUser.access_token && user.access_token){
              sessionStorage.setItem("userAuthenticated", JSON.stringify(user))
              userMerged = user;
            }else if(sessionUser.access_token && !user.access_token){
              userMerged = sessionUser
            }
            return (
              <BrowserRouter>
                <Switch>
                  <Route exact path={Routes.LOGIN} render={() => <Login onLoggedIn={setUserAuthenticated}/>}/>
                  {userMerged.access_token && <Route exact path={Routes.HOME} render={() =>
                    <Main user={userMerged} onLogout={clearUser}/>}/>}
                  <Redirect to={Routes.LOGIN}/>
                </Switch>
              </BrowserRouter>
            )
          }
          }
        </AuthenticatedUserContext.Consumer>
      </AuthenticatedUserContext.Provider>
    </Provider>
  );
}

export default App;