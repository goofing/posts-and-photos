import React from "react";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Styles from './styles';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as Strings from './../../utils/strings';
import {
  showSnackBar,
  hideSnackBar
} from "../../redux/common/actions";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AlertSnackbar = (props) => {
  const { message, showSnackBar, typeMessage } = props.common;
  const classes = Styles();
  const severity = typeMessage === Strings.ERROR_MESSAGE_TYPE? 'error': 'success';
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
    props.dispatchHideSnackBar()
  };

  React.useEffect(() => {
    if (showSnackBar) {
      handleOpen()
    }else {
      handleClose()
    }
  }, [showSnackBar])


  return (
    <div className={classes.root}>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity}>
          { message }
        </Alert>
      </Snackbar>
    </div>
  );
}

const mapStateToProps = (state, props) => {
  const {common} = {...state};
  return {common}
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
  dispatchHideSnackBar: hideSnackBar,
  dispatchShowSnackBar: showSnackBar,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AlertSnackbar);
