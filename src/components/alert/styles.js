import { makeStyles } from '@material-ui/core/styles';

const Styles =  makeStyles((theme) => ({
  root: {
    width: 'auto',
    position: 'absolute',
    bottom: '10%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
export default Styles;