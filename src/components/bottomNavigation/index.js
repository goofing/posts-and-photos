import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import Styles from './styles'

export default function Navigation(props) {
  const classes = Styles();
  const { value, setValue, children } = props;

  return (
    <>
      {children[value]}
      <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction label="POSTS" icon={<SupervisorAccountIcon />} />
        <BottomNavigationAction label="PHOTOS" icon={<PhotoLibraryIcon />} />
      </BottomNavigation>
    </>
  );
}
