import { makeStyles } from '@material-ui/core/styles';
import {blue} from "@material-ui/core/colors";

const Styles =  makeStyles({
  root: {
    backgroundColor: blue[50],
    width: '100%',
    position: 'fixed',
    bottom: '0',
  },
});
export default Styles;