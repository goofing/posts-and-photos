import React from 'react';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Styles from './styles'
import {
  clearFeedback
} from "../../../redux/common/actions";
import {connect} from "react-redux";
import * as FeedbackStates from '../../../utils/feedbackStates';
import {bindActionCreators} from "redux";


const ButtonProgress = function (props) {
  const classes = Styles();
  const {onClick} = props;
  const {feedback} = props.common;
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [error, setError] = React.useState(false);
  const timer = React.useRef();

  const buttonClassname = clsx({
    [classes.buttonSuccess]: success,
    [classes.buttonError]: error,
  });

  const timeForFeedback = 500;

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
      props.dispatchClearFeedback();
    };
  }, []);
  React.useEffect(() => {
    if (!loading){
      timer.current = window.setTimeout(() => {
        setSuccess(false);
        setError(false);
        props.dispatchClearFeedback();
      }, timeForFeedback);
    }

  }, [loading]);

  React.useEffect(() => {
    if (feedback === FeedbackStates.ERROR) {
      setError(true);
    } else if (feedback === FeedbackStates.SUCCESS) {
      setSuccess(true);
    }
    timer.current = window.setTimeout(() => {
      setLoading(false);
    }, timeForFeedback)
  }, [feedback]);

  const handleButtonClick = () => {
    if (!loading) {
      setSuccess(false);
      setError(false);
      setLoading(true);
      onClick()
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <Button
          style={{height: 50}}
          fullWidth
          variant="contained"
          color="primary"
          className={buttonClassname}
          disabled={loading}
          onClick={handleButtonClick}
        >
          {success?'SUCCESSFULL!':error?'ERROR':props.children}

        </Button>
        {loading && <CircularProgress size={24} className={classes.buttonProgress}/>}
      </div>
    </div>
  );
}

const mapStateToProps = (state, props) => {
  const {common} = {...state};
  return {common}
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
  dispatchClearFeedback: clearFeedback,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ButtonProgress)