import React from 'react';
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import IconButton from "@material-ui/core/IconButton";
import Styles from "./styles";
import Button from "@material-ui/core/Button";


const CloseSessionButton = function (props) {
  const style = Styles()
  return (
    <IconButton
      className={style.root} onClick={props.onClick} aria-label="log_out">
      <ExitToAppIcon
        style={{height: 40, width: 40}}
      />
    </IconButton>
  );
}

export default CloseSessionButton;