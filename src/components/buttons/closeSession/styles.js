import { makeStyles } from '@material-ui/core/styles';

const Styles = makeStyles(() => ({
  root: {
    position: 'fixed',
    top: 5,
    right: 35,
  },
}));


export default Styles;