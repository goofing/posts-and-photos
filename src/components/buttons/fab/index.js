import React from 'react';
import Fab from '@material-ui/core/Fab';
import Styles from "./styles";

export default function CustomFab(props) {
  const classes = Styles();
  const { icon,style, disabled, onClick } = props;
  return (
      <Fab style={style}  disabled={disabled} color="primary"  onClick={onClick}>
        {icon}
      </Fab>
  );
}
