import { makeStyles } from '@material-ui/core/styles';
import {red} from "@material-ui/core/colors";

const Styles =  makeStyles((theme) => ({
  root: {
    height: '100%',
    maxWidth: '100%',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));
export default Styles;