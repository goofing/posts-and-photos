import React from 'react';
import CardGrid from './../cardGrid'
import Grid from "@material-ui/core/Grid";


export default function CardGridList(props) {
  const {posts} = props
  return (
    <Grid style={{marginBottom: 100, marginTop:20}}>
      {posts.map((post, i) => {
        return (
          <CardGrid key={i} post={post}/>
        )
      })}
    </Grid>
  );
}
