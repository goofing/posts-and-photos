import React from "react";
import {AccountCircle, Visibility, VisibilityOff} from '@material-ui/icons';
import {Grid, CardContent, Card, TextField} from "@material-ui/core";
import GridStyle from './styles'
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import ButtonProgress from '../buttons/buttonProgress'

const LoginForm = (props) => {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [visiblePassword, isVisible] = React.useState(false)
  const classes = GridStyle()
  const handleClick = props.onSubmit;
  return (
    <Grid container justify={'center'} spacing={6}>
      <Grid className={classes.topCenter} item xs={12} md={4}>
        <Card>
          <CardContent>
            <Grid container justify={'center'} spacing={4}>
              <Grid item xs={6} >
                <AccountCircle style={{padding: 'auto', fontSize: 150}}/>
              </Grid>
              <Grid item xs={12}>
                <TextField fullWidth id="username" onChange={e => setUsername(e.target.value)} label="Username" value={username}/>
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                  <Input
                    id="password"
                    type={visiblePassword ? 'text' : 'password'}
                    value={password}
                    variant={"outlined"}
                    onChange={e => setPassword(e.target.value)}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={ () => isVisible(!visiblePassword)}
                        >
                          {visiblePassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <ButtonProgress onClick={() => handleClick({username, password})} >
                  Login
                </ButtonProgress>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

LoginForm.defaultProps = {
  onSubmit: Function,
};

export default LoginForm;