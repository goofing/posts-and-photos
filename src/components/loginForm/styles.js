import { makeStyles } from '@material-ui/core/styles';

const Styles = makeStyles({
  topCenter: {
    position: 'absolute',
    top: '25%',
    width: '100%'
  },
});

export default Styles;