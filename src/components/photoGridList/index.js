import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import Styles from './styles';
import {Grid} from "@material-ui/core";

export default function PhotoGridList(props) {
  const classes = Styles();
  const {photos} = props

  return (
    <div className={classes.root}>
      <Grid container justify={'center'}>
        <Grid item xs={10}>
          <GridList cellHeight={380} className={classes.gridList}>
            <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
              <ListSubheader component="div">Albums</ListSubheader>
            </GridListTile>
            {photos.map((record, index) => (
              <GridListTile key={index}>
                <img src={record.url} alt={record.title} />
                <GridListTileBar
                  title={record.title}
                  subtitle={<span>Album: {record.albumId}</span>}
                  actionIcon={
                    <IconButton aria-label={`info about ${record.title}`} className={classes.icon}>
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </GridListTile>
            ))}
          </GridList>
        </Grid>
      </Grid>
    </div>
  );
}
