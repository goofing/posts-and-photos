import React from "react";
import Fab from "../components/buttons/fab";
import {Grid} from "@material-ui/core";
import {ArrowForward, ArrowBack} from "@material-ui/icons";

const pagination = (WrappedComponent, user) => {

  const Component = (props) => {
    const [offset, setOffset] = React.useState(0)
    const [isMax, setIsMax] = React.useState(true)
    const [isMin, setIsMin] = React.useState(true)
    const [total, setTotal] = React.useState(-1)

    React.useEffect(() => {
      if(total>0 && offset >= total){
        setIsMax(true)
      } else {
        setIsMax(false)
      }
      if(offset <= 0){
        setIsMin(true)
      } else {
        setIsMin(false)
      }
    }, [offset]);

    return (
      <Grid container >
        <Grid item xs={12} ><WrappedComponent {...props} handleTotal={setTotal} offset={offset}/></Grid>
        <Grid item xs={1}><Fab disabled={isMin} style={{
          position: 'fixed',
          bottom: 65,
          left: 20,
        }} icon={<ArrowBack/>} onClick={() => offset > 0 ? setOffset(offset - 1) : null}>
          Anterior
        </Fab></Grid>
        <Grid item xs={1}><Fab disabled={isMax} style={{
          position: 'fixed',
          bottom: 65,
          right: 35,
        }} icon={<ArrowForward/>} onClick={() => setOffset(offset + 1)}>
          Siguiente
        </Fab></Grid>
      </Grid>
    )
  };

  return Component;
}

export default pagination;