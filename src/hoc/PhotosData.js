import React from "react";
import {getPhotos} from "../services/photos";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {showSnackBar} from "../redux/common/actions";
import * as Strings from "../utils/strings";

const photosData = (WrappedComponent, user) => {
  const Component = (props) => {
    const [photos, setPhotos] = React.useState([])
    const { offset, handleTotal } = props

    const handleRetrievePhotos = (photos) => {
      setPhotos(photos.data.records);
      handleTotal(photos.data.total)
    }

    const handleError = () => {
      props.dispatchShowSnackBar('Error obteniendo datos', Strings.ERROR_MESSAGE_TYPE)
    }

    React.useEffect(() => {
      getPhotos(handleRetrievePhotos, handleError)(user.access_token, 20, offset);
      }, [offset]);

    return (
      <WrappedComponent {...props} photos={photos}/>
    )
  };

  const mapStateToProps = (state, props) => {
    const {common} = {...state};
    return {common}
  }
  const mapDispatchToProps = (dispatch) => bindActionCreators({
    dispatchShowSnackBar: showSnackBar,
  }, dispatch);

  return connect(mapStateToProps, mapDispatchToProps)(Component);
}

export default photosData;