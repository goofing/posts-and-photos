import React from "react";
import {getPost} from "../services/posts";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {showSnackBar} from "../redux/common/actions";
import Fab from "../components/buttons/fab";
import * as Strings from "../utils/strings";
import {Grid} from "@material-ui/core";
import {ArrowForward, ArrowBack} from "@material-ui/icons";

const photosPostData = (WrappedComponent, user) => {

  const Component = (props) => {
    const [posts, setPosts] = React.useState([])
    const { offset, handleTotal } = props


    const handleRetrievePost = (posts) => {
      setPosts(posts.data.records);
      handleTotal(posts.data.total)
    }

    const handleError = () => {
      props.dispatchShowSnackBar('Error obteniendo datos', Strings.ERROR_MESSAGE_TYPE)
    }

    React.useEffect(() => {
      getPost(handleRetrievePost, handleError)(user.access_token, 20, offset);
    }, [offset]);

    return (
      <WrappedComponent {...props} posts={posts}/>
    )
  };

  return Component;
}

export default photosPostData;