import React from "react";
import Alert from '../components/alert'

const withSnackBar = (WrappedComponent) => {

  return (props) => {
    return ( <>
      <Alert/>
      <WrappedComponent {...props}/>
    </>)
  }
}


export default withSnackBar