import * as commonTypes from './actionsTypes';

export const showSnackBar = (message, typeMessage) => {
    return {
        type: commonTypes.SHOW_SNACKBAR,
        message,typeMessage
    }
};

export const hideSnackBar = () => {
    return {
        type: commonTypes.HIDE_SNACKBAR,
    }
}

export const setLoadingOn = () => {
    return {
        type: commonTypes.LOADING_ON,
    }
}

export const setLoadingOff = () => {
    return {
        type: commonTypes.LOADING_OFF,
    }
}

export const setSuccessFeedback = () => {
    return {
        type: commonTypes.SUCCESS_FEEDBACK,
    }
}

export const setErrorFeedback = () => {
    return {
        type: commonTypes.ERROR_FEEDBACK,
    }
}

export const clearFeedback = () => {
    return {
        type: commonTypes.CLEAR_FEEDBACK,
    }
}