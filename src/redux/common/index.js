import * as actionsTypes from './actionsTypes';
import * as FeedbackState from './../../utils/feedbackStates'

export const initialState = {

  showLoading: false,
  message: '',
  showSnackBar: false,
  typeMessage: '',
  feedback: ''
};

export const reducer = (state = initialState, action) => {
  const {type} = action;
  switch (type) {

    case actionsTypes.LOADING_ON: {
      let showLoading = true;
      return {...state, showLoading};

    }
    case actionsTypes.LOADING_OFF: {
      let showLoading = false;
      // let {message} = action.payload;
      return {...state, showLoading};
    }

    case actionsTypes.SHOW_SNACKBAR: {
      let showSnackBar = true;
      let message = action.message;
      let typeMessage = action.typeMessage;
      return {...state, showSnackBar, message, typeMessage};
    }

    case actionsTypes.HIDE_SNACKBAR: {
      let showSnackBar = false;
      return {...state, showSnackBar};
    }


    case actionsTypes.ERROR_FEEDBACK: {
      const feedback = FeedbackState.ERROR;
      return {...state, feedback};
    }

    case actionsTypes.SUCCESS_FEEDBACK: {
      const feedback = FeedbackState.SUCCESS;
      return {...state, feedback};
    }

    case actionsTypes.CLEAR_FEEDBACK: {
      const feedback = '';
      return {...state, feedback};
    }


    default:
      return state
  }

}