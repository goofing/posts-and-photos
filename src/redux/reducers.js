import {combineReducers} from 'redux';
import {reducer as common} from './common';

export default combineReducers({
    common,
})
