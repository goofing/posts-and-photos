import HttpBuilder from "../utils/httpBuilder";
const url = 'http://localhost:8080/core-test'

export const authenticate = (onSuccess, onError) => {
  return (data) => {
    const httpBuilder = new HttpBuilder()
    const request = httpBuilder.post(`${url}/authenticate`,data);
    request.execute(onSuccess, onError)
  }
}