import HttpBuilder from "../utils/httpBuilder";
const url = 'http://localhost:8080/core-test'

export const getPhotos = (onSuccess, onError) => {
  return (token, limit = 10, offset = 0) => {
    const httpBuilder = new HttpBuilder()
    const request = httpBuilder.get(`${url}/photo?limit=${limit}&offset=${offset}`, token);
    request.execute(onSuccess, onError)
  }
}