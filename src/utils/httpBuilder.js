import HttpRequest from './httpRequest';

const HttpBuilder = function() {
  const httpRequest = new HttpRequest();

  const getHeaders = (token) => {
    const obj = {};
    if (token) {
      obj['Authorization'] = `Bearer ${token}`;
    }

    return obj;
  };

  this.get = (url, token = '') => {
    const headers = getHeaders(token);

    return httpRequest.prepare({
      method: 'get',
      url: url,
      timeout: 3000,
      headers
    });
  };

  this.post = (url, data, token = '') => {
    const headers = getHeaders(token);

    return httpRequest.prepare({
      method: 'post',
      url: url,
      timeout: 3000,
      data,
      headers
    });
  };
};


export default HttpBuilder;