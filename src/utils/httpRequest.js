import axios from 'axios'


const HttpRequest = function () {
  this.prepare = (options) => ({
    execute: (onSuccess, onError) => { // eslint-disable-next-line no-useless-catch
      axios(options).then(response => {
        onSuccess(response.data)
      }).catch(e => {
        onError(e.response.data)
      });
    }
  });
};


export default HttpRequest