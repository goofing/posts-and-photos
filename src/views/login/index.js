import React from "react";
import {withRouter} from 'react-router-dom'

import LoginForm from "../../components/loginForm";
import * as Routes from './../../routes';
import * as Strings from './../../utils/strings';
import { authenticate } from "../../services/authentication";
import withSnackBar from "../../hoc/SnackBar";
import {bindActionCreators} from "redux";
import {
  showSnackBar,
  setLoadingOff,
  setLoadingOn,
  setErrorFeedback,
  setSuccessFeedback, clearFeedback
} from "../../redux/common/actions";
import {connect} from "react-redux";

const Login = (props) => {
  const timer = React.useRef();

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  const handleAuthenticated = (data) => {
    props.dispatchFeedbackSuccess();
    props.onLoggedIn(data)
    timer.current = window.setTimeout(() => {
      props.history.push(Routes.HOME)
    }, 1000);

  }

  const handleError = () => {
    props.dispatchFeedbackError();
    props.dispatchShowSnackBar('Credenciales invalidas', Strings.ERROR_MESSAGE_TYPE )
  }

  return(
      <LoginForm onSubmit={authenticate(handleAuthenticated, handleError)}/>
  )
}
const mapStateToProps = (state, props) => {
  const {common} = {...state};
  return {common}
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
  dispatchShowSnackBar: showSnackBar,
  dispatchFeedbackError: setErrorFeedback,
  dispatchFeedbackSuccess: setSuccessFeedback,
  dispatchClearFeedback: clearFeedback,

}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withSnackBar(Login)));
