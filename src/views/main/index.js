import React from "react";
import {withRouter} from 'react-router-dom'
import withSnackBar from "../../hoc/SnackBar";
import NavBar from "../../components/bottomNavigation";
import Grid from "@material-ui/core/Grid";
import CloseSessionButton from './../../components/buttons/closeSession'
import PhotoGridList from "../../components/photoGridList";
import CardGridList from "../../components/cardGridList";
import WithPhotosData from "../../hoc/PhotosData"
import WithPostsData from "../../hoc/PostData"
import Pagination from "../../hoc/Pagination"


const Main = (props) => {
  const [page, setPage] = React.useState(0);
  const PhotoComponent = Pagination(WithPhotosData(PhotoGridList, props.user));
  const PostsComponent = Pagination(WithPostsData(CardGridList, props.user));
  return(
      <Grid container>
        <CloseSessionButton onClick={()=> props.onLogout()}/>
        <NavBar value={page} setValue={setPage}>
          <PostsComponent/>
          <PhotoComponent/>
        </NavBar>
      </Grid>
  )
}

export default withRouter(withSnackBar(Main));
